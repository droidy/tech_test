package uk.co.droidy.tech_test.detail

interface DetailContract {
    interface View {
        fun setData(schoolDetailModel: SchoolDetailModel)
        fun showError(message: String)
    }

    interface Presenter {
        fun setData(detailDbn: String)
        fun onPause()
    }
}