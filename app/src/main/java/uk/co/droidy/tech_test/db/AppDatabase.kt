package uk.co.droidy.tech_test.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import uk.co.droidy.tech_test.api.dto.SchoolDto


@Database(entities = [SchoolDto::class], version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun getSchoolDtoDao(): SchoolDtoDao

    companion object {
        const val APP_DB_NAME = "app_db"
    }

}