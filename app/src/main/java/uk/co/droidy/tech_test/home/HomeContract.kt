package uk.co.droidy.tech_test.home

import uk.co.droidy.tech_test.home.school_list.SchoolModel

interface HomeContract {
    interface View {
        fun initView(data: List<SchoolModel>)
        fun showError(message: String)
    }

    interface Presenter {
        fun init()
        fun onPause()
    }
}