package uk.co.droidy.tech_test.api

import io.reactivex.Single
import retrofit2.http.GET
import uk.co.droidy.tech_test.api.dto.SchoolDetail
import uk.co.droidy.tech_test.api.dto.SchoolDto

interface ApiService {

    @GET("/resource/s3k6-pzi2.json")
    fun getSchools(): Single<List<SchoolDto>>

    @GET("/resource/f9bf-2cp4.json")
    fun getSchoolDetails(): Single<List<SchoolDetail>>

}