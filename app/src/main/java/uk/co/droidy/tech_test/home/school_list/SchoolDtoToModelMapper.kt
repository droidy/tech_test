package uk.co.droidy.tech_test.home.school_list

import io.reactivex.functions.Function
import uk.co.droidy.tech_test.api.dto.SchoolDto
import javax.inject.Inject


class SchoolDtoToModelMapper
@Inject constructor() : Function<List<SchoolDto>, List<SchoolModel>> {
    override fun apply(t: List<SchoolDto>): List<SchoolModel> {

        val schoolList = ArrayList<SchoolModel>()
        for (school in t) {
            schoolList.add(
                SchoolModel(school.dbn, school.schoolName)
            )
        }

        return schoolList
    }

}