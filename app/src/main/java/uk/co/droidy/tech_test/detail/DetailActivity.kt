package uk.co.droidy.tech_test.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail.*
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.home.school_list.SchoolModel
import javax.inject.Inject

class DetailActivity : AppCompatActivity(), DetailContract.View {

    @Inject lateinit var presenter: DetailContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        presenter.setData(intent.getStringExtra(ITEM))
    }

    override fun setData(schoolDetailModel: SchoolDetailModel) {
        detail_title_tv.text = schoolDetailModel.schoolName
        detail_sat_math_tv.text = schoolDetailModel.satMathScore
        detail_sat_writing_tv.text = schoolDetailModel.satWritingScore
        detail_sat_reading_tv.text = schoolDetailModel.satReadingScore
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }

    companion object {
        const val ITEM = "ITEM"
    }
}
