package uk.co.droidy.tech_test.common.di

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import uk.co.droidy.tech_test.home.HomeActivity
import uk.co.droidy.tech_test.home.HomeContract
import uk.co.droidy.tech_test.common.App
import uk.co.droidy.tech_test.db.AppDatabase
import uk.co.droidy.tech_test.common.resources.StringResource
import uk.co.droidy.tech_test.common.resources.StringResourceWrapper
import uk.co.droidy.tech_test.common.scheduler.AppScheduler
import uk.co.droidy.tech_test.common.scheduler.SchedulerWrapper
import uk.co.droidy.tech_test.detail.DetailActivity
import uk.co.droidy.tech_test.detail.DetailContract
import uk.co.droidy.tech_test.detail.di.DetailModule
import uk.co.droidy.tech_test.home.di.HomeModule
import javax.inject.Singleton

@Suppress("unused")
@Module
abstract class AppModule {

    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun contributeHomeActivity(): HomeActivity

    @Binds
    abstract fun bindHomeView(homeActivity: HomeActivity): HomeContract.View

    @ContributesAndroidInjector(modules = [DetailModule::class])
    abstract fun contributeDetailActivity(): DetailActivity

    @Binds
    abstract fun bindDetailView(detailActivity: DetailActivity): DetailContract.View

    @Binds
    abstract fun bindContext(app: App): Context

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideScheduler(): AppScheduler {
            return SchedulerWrapper.getInstance()
        }

        @JvmStatic
        @Provides
        fun provideResources(context: Context): StringResource {
            return StringResourceWrapper(context)
        }

        @JvmStatic
        @Provides
        fun provideCompositeDisposable(): CompositeDisposable {
            return CompositeDisposable()
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideAppDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.APP_DB_NAME)
                .build()
        }
    }

}