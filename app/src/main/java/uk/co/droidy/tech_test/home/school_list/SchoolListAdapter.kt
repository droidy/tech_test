package uk.co.droidy.tech_test.home.school_list

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.detail.DetailActivity
import javax.inject.Inject

class SchoolListAdapter
@Inject constructor(): RecyclerView.Adapter<SchoolViewHolder>() {

    private var dataList: List<SchoolModel> = ArrayList()

    fun setData(data: List<SchoolModel>) {
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.article_item, parent, false)

        return SchoolViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {

        holder.bind(dataList[position], View.OnClickListener {
            val intent = Intent(it.context, DetailActivity::class.java)
            intent.putExtra(DetailActivity.ITEM, dataList[position].dbn)
            it.context.startActivity(intent)
        })
    }

    override fun getItemCount(): Int = dataList.size

}