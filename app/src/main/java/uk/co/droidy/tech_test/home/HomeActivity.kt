package uk.co.droidy.tech_test.home

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_home.*
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.home.school_list.SchoolListAdapter
import uk.co.droidy.tech_test.home.school_list.SchoolModel
import javax.inject.Inject

class HomeActivity : AppCompatActivity(), HomeContract.View {

    @Inject lateinit var presenter: HomeContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        presenter.init()
    }

    override fun initView(data: List<SchoolModel>) {
        home_rv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val adapter = SchoolListAdapter()
        adapter.setData(data)
        home_rv.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        home_rv.adapter = adapter
    }

    override fun showError(message: String) {
        AlertDialog.Builder(this)
            .setTitle(R.string.global_default_error_heading)
            .setMessage(message)
            .setPositiveButton(getString(R.string.global_ok_uppercase), null)
            .create()
            .show()
    }
}
