package uk.co.droidy.tech_test.api

import io.reactivex.Single
import uk.co.droidy.tech_test.api.dto.SchoolDetail
import uk.co.droidy.tech_test.api.dto.SchoolDto
import javax.inject.Inject

class ApiInteractor
@Inject constructor(private val apiService: ApiService) {

    fun getSchools(): Single<List<SchoolDto>> {
        return Single.defer {
            return@defer apiService.getSchools()
        }
    }

    fun getSchoolDetails(): Single<List<SchoolDetail>> {
        return Single.defer {
            return@defer apiService.getSchoolDetails()
        }
    }
}