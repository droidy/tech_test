package uk.co.droidy.tech_test.detail

import android.annotation.SuppressLint
import android.util.Log
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.api.ApiInteractor
import uk.co.droidy.tech_test.common.extensions.addTo
import uk.co.droidy.tech_test.common.resources.StringResource
import uk.co.droidy.tech_test.common.scheduler.AppScheduler
import javax.inject.Inject

class DetailPresenter
@Inject constructor(
    private val view: DetailContract.View,
    private val apiInteractor: ApiInteractor,
    private val stringResource: StringResource,
    private val compositeDisposable: CompositeDisposable,
    private val scheduler: AppScheduler
) : DetailContract.Presenter {

    @SuppressLint("CheckResult")
    override fun setData(detailDbn: String) {
        apiInteractor.getSchoolDetails()
            .subscribeOn(scheduler.io())
            .toObservable()
            .flatMap {
                Observable.fromIterable(it)
            }
            .filter {
                it.dbn == detailDbn
            }
            .map {//todo move to a mapper
                SchoolDetailModel(
                    it.satMathScore,
                    it.satWritingScore,
                    it.satReadingScore,
                    it.schoolName
                )
            }
            .observeOn(scheduler.mainThread())
            .subscribe({
                view.setData(it)
            }, {
                view.showError(it.message ?: stringResource.getStringFromId(R.string.global_default_error_message))
            })
            .addTo(compositeDisposable)
    }

    override fun onPause() {
        compositeDisposable.clear()
    }

}