package uk.co.droidy.tech_test.db

import io.reactivex.Maybe
import uk.co.droidy.tech_test.api.dto.SchoolDto
import javax.inject.Inject

class DbInteractor
@Inject constructor(private val appDatabase: AppDatabase) {

    fun insertSchoolsCall(schools: List<SchoolDto>): Array<Long> {
        return appDatabase.getSchoolDtoDao().insertSchools(schools)
    }

    fun getSchools(): Maybe<List<SchoolDto>> {
        return Maybe.fromCallable {
            return@fromCallable appDatabase.getSchoolDtoDao().getSchools()
        }
    }
}