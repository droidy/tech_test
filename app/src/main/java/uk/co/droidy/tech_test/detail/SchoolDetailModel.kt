package uk.co.droidy.tech_test.detail

data class SchoolDetailModel(
    val satMathScore: String,
    val satWritingScore: String,
    val satReadingScore: String,
    val schoolName: String
)