package uk.co.droidy.tech_test.api.dto

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class SchoolDto(
    @PrimaryKey val dbn: String,
    @SerializedName("school_name") val schoolName: String
)

data class SchoolDetail(
    val dbn: String,
    @SerializedName("sat_math_avg_score") val satMathScore: String,
    @SerializedName("sat_writing_avg_score") val satWritingScore: String,
    @SerializedName("sat_critical_reading_avg_score") val satReadingScore: String,
    @SerializedName("school_name") val schoolName: String
)