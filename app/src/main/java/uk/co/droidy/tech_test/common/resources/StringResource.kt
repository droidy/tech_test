package uk.co.droidy.tech_test.common.resources

import android.support.annotation.StringRes


interface StringResource {
    fun getStringFromId(@StringRes stringId: Int): String

    fun getStringFromId(@StringRes stringId: Int, formatArgs: Any): String
}