package uk.co.droidy.tech_test.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import uk.co.droidy.tech_test.api.dto.SchoolDto

@Dao
interface SchoolDtoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertSchools(schools: List<SchoolDto>): Array<Long>

    @Query("SELECT * FROM SchoolDto")
    fun getSchools(): List<SchoolDto>

}