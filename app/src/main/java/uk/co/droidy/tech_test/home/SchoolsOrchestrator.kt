package uk.co.droidy.tech_test.home

import io.reactivex.Single
import uk.co.droidy.tech_test.api.ApiInteractor
import uk.co.droidy.tech_test.api.dto.SchoolDto
import uk.co.droidy.tech_test.db.DbInteractor
import javax.inject.Inject

@Suppress("UnstableApiUsage")
class SchoolsOrchestrator
@Inject constructor(
    private val apiInteractor: ApiInteractor,
    private val dbInteractor: DbInteractor
) {
    fun getSchools(): Single<List<SchoolDto>> {
        return Single.defer {
            return@defer dbInteractor.getSchools()
                .filter {
                    !it.isEmpty()
                }
                .switchIfEmpty(
                    apiInteractor.getSchools()
                        .doOnSuccess {
                            dbInteractor.insertSchoolsCall(it)
                        }
                )
        }
    }
}