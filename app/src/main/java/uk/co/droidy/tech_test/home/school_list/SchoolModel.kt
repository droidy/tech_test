package uk.co.droidy.tech_test.home.school_list

data class SchoolModel(
    val dbn: String,
    val schoolName: String
)