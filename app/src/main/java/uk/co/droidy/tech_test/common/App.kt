package uk.co.droidy.tech_test.common

import android.app.Activity
import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import uk.co.droidy.tech_test.common.di.DaggerAppComponent
import javax.inject.Inject

class App: Application(), HasActivityInjector {
    @Inject
    lateinit var injector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = injector

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
            .app(this)
            .build()
            .inject(this)

        Fresco.initialize(this)
        Stetho.initializeWithDefaults(this)
    }
}