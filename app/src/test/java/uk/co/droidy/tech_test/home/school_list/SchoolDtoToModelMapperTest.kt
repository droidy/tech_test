package uk.co.droidy.tech_test.home.school_list

import com.flextrade.jfixture.FixtureAnnotations
import com.flextrade.jfixture.annotations.Fixture
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import uk.co.droidy.tech_test.api.dto.SchoolDto

class SchoolDtoToModelMapperTest {
    @Fixture lateinit var fixtItemsDTO: List<SchoolDto>

    private lateinit var sut: SchoolDtoToModelMapper

    @Before
    fun setUp() {
        FixtureAnnotations.initFixtures(this)

        sut = SchoolDtoToModelMapper()
    }

    @Test
    fun apply() {

        val actualResult = sut.apply(fixtItemsDTO)

        for ((i, value) in fixtItemsDTO.withIndex()) {
            assertEquals(fixtItemsDTO[i].dbn, actualResult[i].dbn)
            assertEquals(fixtItemsDTO[i].schoolName, actualResult[i].schoolName)
        }
    }
}