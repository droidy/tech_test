package uk.co.droidy.tech_test.test_utils

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import uk.co.droidy.tech_test.common.scheduler.AppScheduler

class TestAppScheduler: AppScheduler {
    override fun io(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun mainThread(): Scheduler {
        return Schedulers.trampoline()
    }
}