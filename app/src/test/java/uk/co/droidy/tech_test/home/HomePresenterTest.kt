package uk.co.droidy.tech_test.home

import com.flextrade.jfixture.FixtureAnnotations
import com.flextrade.jfixture.annotations.Fixture
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.api.dto.SchoolDto
import uk.co.droidy.tech_test.common.resources.StringResource
import uk.co.droidy.tech_test.home.school_list.SchoolDtoToModelMapper
import uk.co.droidy.tech_test.home.school_list.SchoolModel
import uk.co.droidy.tech_test.test_utils.TestAppScheduler

const val ANY_DEFAULT_ERROR_STRING = "ANY_DEFAULT_ERROR_STRING"
const val ANY_SERVER_ERROR_MESSAGE = "ANY_SERVER_ERROR_MESSAGE"

class HomePresenterTest {
    private val scheduler = TestAppScheduler()

    @Fixture lateinit var fixtListSchoolDto: List<SchoolDto>
    @Fixture lateinit var fixtListSchoolModel: List<SchoolModel>

    @Mock lateinit var mockView: HomeContract.View
    @Mock lateinit var mockSchoolsOrchestrator: SchoolsOrchestrator
    @Mock lateinit var mockSchoolDtoToModelMapper: SchoolDtoToModelMapper
    @Mock lateinit var mockStringResource: StringResource
    @Mock lateinit var mockCompositeDisposable: CompositeDisposable

    private lateinit var sut: HomePresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        FixtureAnnotations.initFixtures(this)

        sut = HomePresenter(mockView, mockSchoolsOrchestrator, mockSchoolDtoToModelMapper, mockStringResource, mockCompositeDisposable, scheduler)
    }

    @Test
    fun init_success() {
        //setup
        whenever(mockSchoolsOrchestrator.getSchools()).thenReturn(Single.just(fixtListSchoolDto))
        whenever(mockSchoolDtoToModelMapper.apply(fixtListSchoolDto)).thenReturn(fixtListSchoolModel)

        //run
        sut.init()

        //verify
        verify(mockSchoolsOrchestrator).getSchools()
        verify(mockView).initView(fixtListSchoolModel)
    }

    @Test
    fun init_showDefaultErrorMessage() {
        //setup
        whenever(mockStringResource.getStringFromId(R.string.global_default_error_message)).thenReturn(ANY_DEFAULT_ERROR_STRING)
        whenever(mockSchoolsOrchestrator.getSchools()).thenReturn(Single.error(Throwable()))

        //run
        sut.init()

        //verify
        verify(mockView).showError(ANY_DEFAULT_ERROR_STRING)
    }

    @Test
    fun init_showServerErrorMessage() {
        //setup
        whenever(mockSchoolsOrchestrator.getSchools()).thenReturn(Single.error(Throwable(ANY_SERVER_ERROR_MESSAGE)))

        //run
        sut.init()

        //verify
        verify(mockView).showError(ANY_SERVER_ERROR_MESSAGE)
    }

    @Test
    fun onPause_disposeCalls() {
        //setup
        whenever(mockSchoolsOrchestrator.getSchools()).thenReturn(Single.just(fixtListSchoolDto))
        whenever(mockSchoolDtoToModelMapper.apply(fixtListSchoolDto)).thenReturn(fixtListSchoolModel)
        sut.init()

        //run
        sut.onPause()

        //verify
        verify(mockCompositeDisposable).clear()
    }
}