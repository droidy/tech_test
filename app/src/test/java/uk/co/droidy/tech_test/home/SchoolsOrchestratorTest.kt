package uk.co.droidy.tech_test.home

import com.flextrade.jfixture.FixtureAnnotations
import com.flextrade.jfixture.annotations.Fixture
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import uk.co.droidy.tech_test.api.ApiInteractor
import uk.co.droidy.tech_test.api.dto.SchoolDto
import uk.co.droidy.tech_test.db.DbInteractor

class SchoolsOrchestratorTest {
    @Fixture lateinit var fixtApiItemsDto: List<SchoolDto>
    @Fixture lateinit var fixtDbItemsDTO: List<SchoolDto>

    @Fixture lateinit var fixtInsertionResults: Array<Long>

    @Mock lateinit var mockApiInteractor: ApiInteractor
    @Mock lateinit var mockDbInteractor: DbInteractor

    private lateinit var sut: SchoolsOrchestrator

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        FixtureAnnotations.initFixtures(this)

        sut = SchoolsOrchestrator(mockApiInteractor, mockDbInteractor)
    }

    @Test
    fun tryToGetSchoolsFromDb_switchToNetworkApiIfEmpty() {
        whenever(mockDbInteractor.getSchools()).thenReturn(Maybe.empty())
        whenever(mockApiInteractor.getSchools()).thenReturn(Single.just(fixtApiItemsDto))
        whenever(mockDbInteractor.insertSchoolsCall(fixtApiItemsDto)).thenReturn(fixtInsertionResults)


        sut.getSchools()
            .test()
            .assertSubscribed()
            .assertComplete()
            .assertNoErrors()
            .assertResult(fixtApiItemsDto)


        verify(mockDbInteractor).getSchools()
        verify(mockApiInteractor).getSchools()
        verify(mockDbInteractor).insertSchoolsCall(fixtApiItemsDto)
    }

    @Test
    fun getSchoolsFromDb() {
        whenever(mockDbInteractor.getSchools()).thenReturn(Maybe.just(fixtDbItemsDTO))
        whenever(mockApiInteractor.getSchools()).thenReturn(Single.never())
        whenever(mockDbInteractor.insertSchoolsCall(fixtDbItemsDTO)).thenReturn(fixtInsertionResults)

        sut.getSchools()
            .test()
            .assertSubscribed()
            .assertComplete()
            .assertNoErrors()
            .assertResult(fixtDbItemsDTO)

        verify(mockDbInteractor).getSchools()
        verify(mockDbInteractor, never()).insertSchoolsCall(fixtDbItemsDTO)
    }
}