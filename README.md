# Tech Test
- This project uses Google-style MVP architecture with Dagger 2 DI and Dagger 2 AndroidInjection
- For unit test I've used Mockito, Fixture and Mockito-Kotlin (minimum necessary libraries)

## Structure

### Api package
All necessary classes for API communication: interactors, DTOs and related Dagger module.

### Common package
Contains classes used across the app: App class that extends Application, base DI component and main DI module, resource and schedule wrappers.

### Db package
Entire ROOM DB setup (except DAO) lives here.

### Home package
- Contains HomeActivity with main screen, related models, dao and SchoolsOrchestrator.
- At first, SchoolsOrchestrator tries to interrogate the DB. In case data is missing it will make another call to remote API, pass it to the presenter for display and save it into DB at the same time.

## Tests

### Unit tests coverage
- HomePresenter
- SchoolsOrchestrator
- SchoolsDtoToModelMapper


## Possible Improvements
- Adding UI Tests
- Moving all DetailPresenter Rx operator lambdas to separate classes
